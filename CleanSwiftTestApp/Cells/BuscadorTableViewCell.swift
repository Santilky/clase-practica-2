//
//  BuscadorTableViewCell.swift
//  CleanSwiftTestApp
//
//  Created by Santiago Linietsky on 27/01/2022.
//

import UIKit

class BuscadorTableViewCell: UITableViewCell {
    static let identifier = "BuscadorTableViewCell"
    @IBOutlet weak var searcButton: UIButton!
    @IBOutlet weak var textInput: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    func setup() {
        textInput.placeholder = "Inserte lo que desea buscar"
        searcButton.setTitle("Buscar", for: .normal)
        searcButton.addTarget(self, action: #selector(dismissKeyboard), for: .touchDown)
    }
    @objc func dismissKeyboard() {
        self.contentView.endEditing(true)
    }
    
}
