//
//  GenericCellTableViewCell.swift
//  CleanSwiftTestApp
//
//  Created by Santiago Linietsky on 27/01/2022.
//

import UIKit

class GenericCellTableViewCell: UITableViewCell {
    static let identifier = "GenericCellTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var duration: UILabel!
    
    @IBOutlet weak var songName: UILabel!
    
}
