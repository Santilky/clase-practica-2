//
//  MainClassModels.swift
//  CleanSwiftTestApp
//
//  Created by Santiago Linietsky on 27/01/2022.
//

import UIKit

enum MainClassModel {
    enum TableSections: Int, CaseIterable {
        case seccionUno
        case seccionDos
    }
}
