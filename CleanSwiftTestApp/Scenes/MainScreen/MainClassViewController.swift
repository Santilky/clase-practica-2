//
//  MainClassViewController.swift
//  CleanSwiftTestApp
//
//  Created by Santiago Linietsky on 27/01/2022.
//

import UIKit
import SketchKit

class MainClassViewController: UIViewController {
    var viewModel: MainClassViewModel = .init()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "MainClassViewController", bundle: nibBundleOrNil)
        setup()
    }
    func setup() {
        print(MainClassModel.TableSections.seccionUno.rawValue)
        guard let value: MainClassModel.TableSections = .init(rawValue: 0) else {
            return
        }
        print(value)
    }
    override func viewDidLoad() {
       super.viewDidLoad()
        setupUI()
    }
    func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: BuscadorTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: BuscadorTableViewCell.identifier)
        tableView.register(UINib(nibName: GenericCellTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: GenericCellTableViewCell.identifier)
        let newLabel = UILabel()
        view.addSubview(newLabel)
        newLabel.text = "v1.0"
        newLabel.layout.applyConstraint {
            label in
            label.topAnchor(equalTo: self.view.topAnchor, constant: 30)
            label.leftAnchor(equalTo: self.view.leftAnchor, constant: 20)
        }
    }
    // MARK: - IBOutlets
    

    @IBOutlet weak var tableView: UITableView!
    

}
extension MainClassViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section: MainClassModel.TableSections = .init(rawValue: section) else {
            return 0
        }
        switch section {
        case .seccionUno:
            return 1
        case .seccionDos:
            return viewModel.info.count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        MainClassModel.TableSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section: MainClassModel.TableSections = .init(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch section {
            case .seccionUno:
                let cell: BuscadorTableViewCell = tableView.dequeueReusableCell(withIdentifier: BuscadorTableViewCell.identifier) as! BuscadorTableViewCell
                return cell
        case .seccionDos:
            let cell: GenericCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: GenericCellTableViewCell.identifier) as! GenericCellTableViewCell
                cell.songName.text = viewModel.info[indexPath.row].name
                cell.duration.text = viewModel.info[indexPath.row].duration
                return cell
            }
    }
}
